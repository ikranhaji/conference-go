from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def getCityPhoto(city):
    headers = {"Authorization":PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {
        "query": city
    }

    response =  requests.get(url, params=params, headers=headers)
    content = response.json()
    return {"image_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    geo_params = {
        "q": f"{city},{state}, US",
        "limit": 1,
        "appid":OPEN_WEATHER_API_KEY
    }
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geo_url, params=geo_params)
    content = response.json()
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    response = requests.get(weather_url, params=weather_params)
    content = response.json()

    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]

    return {"description":description, "temp": temp}
